#!/bin/sh

docker run -d --name rethinkdb-test -p 30001:28015 rethinkdb:2.4.1-buster-slim rethinkdb --bind all --initial-password 123456
docker run -d --name redis-test -p 30002:6379 redis:6.2.5-alpine3.14
docker run -d --name smtp-test -p 30003:8025 -e REDIS_URL=redis://172.17.0.1:30002/4 \
    registry.gitlab.com/public.eyja.dev/mock-smtp-server:latest \
    python -m aiosmtpd -c mock_smtp_server.RedisSMTPBackend -l 0.0.0.0:8025 -n

#sleep 5

python -W ignore::DeprecationWarning -m unittest

docker stop rethinkdb-test && docker rm rethinkdb-test
docker stop redis-test && docker rm redis-test
docker stop smtp-test && docker rm smtp-test